<?php

declare(strict_types=1);

use PhpCsFixer\Finder;
use PhpCsFixer\Config;

$finder = Finder::create()
    ->in([
        __DIR__ . '/lib',
        __DIR__ . '/scripts',
        __DIR__ . '/tests',
    ]);

$config = new Config();

return $config
    ->setRules([
        '@PSR12' => true,
        // alias
        'backtick_to_shell_exec' => true,
        // array
        'array_syntax' => true,
        'no_multiline_whitespace_around_double_arrow' => true,
        'no_trailing_comma_in_singleline_array' => true,
        'no_whitespace_before_comma_in_array' => true,
        'normalize_index_brace' => true,
        'trim_array_spaces' => true,
        'whitespace_after_comma_in_array' => true,
        // basic
        'braces' => true,
        'encoding' => true,
        // casing
        'constant_case' => true,
        'lowercase_keywords' => true,
        'lowercase_static_reference' => true,
        'magic_method_casing' => true,
        'native_function_casing' => true,
        'native_function_type_declaration_casing' => true,
        // cast
        'cast_spaces' => [
            'space' => 'none',
        ],
        'lowercase_cast' => true,
        'no_unset_cast' => true,
        'short_scalar_cast' => true,
        // class
        'class_attributes_separation' => [
            'elements' => [
                'method' => 'one',
                'trait_import' => 'one',
            ],
        ],
        'class_definition' => true,
        'no_blank_lines_after_class_opening' => true,
        'self_static_accessor' => true,
        'single_class_element_per_statement' => true,
        'single_trait_insert_per_statement' => true,
        'visibility_required' => true,
        'no_trailing_whitespace_in_comment' => true,
        'single_line_comment_style' => true,
        // control structure
        'elseif' => true,
        'include' => true,
        'no_alternative_syntax' => true,
        'no_break_comment' => true,
        'no_superfluous_elseif' => true,
        'no_trailing_comma_in_list_call' => true,
        'no_unneeded_control_parentheses' => true,
        'no_unneeded_curly_braces' => true,
        'no_useless_else' => true,
        'simplified_if_return' => true,
        'switch_case_semicolon_to_colon' => true,
        'switch_case_space' => true,
        'switch_continue_to_break' => true,
        'trailing_comma_in_multiline' => [
            'after_heredoc' => true,
            'elements' => [
                'arrays',
                'arguments',
            ],
        ],
        // function
        'function_declaration' => true,
        'function_typehint_space' => true,
        'lambda_not_used_import' => true,
        'method_argument_space' => true,
        'no_spaces_after_function_name' => true,
        'return_type_declaration' => true,
        // import
        'fully_qualified_strict_types' => true,
        'global_namespace_import' => true,
        'no_leading_import_slash' => true,
        'no_unused_imports' => true,
        'ordered_imports' => true,
        'single_import_per_statement' => true,
        'single_line_after_imports' => true,
        // language construct
        'combine_consecutive_issets' => true,
        'combine_consecutive_unsets' => true,
        'explicit_indirect_variable' => true,
        'single_space_after_construct' => true,
        // list syntax
        'list_syntax' => true,
        // namespace
        'blank_line_after_namespace' => true,
        'clean_namespace' => true,
        'no_leading_namespace_whitespace' => true,
        'single_blank_line_before_namespace' => true,
        // operator
        'concat_space' => [
            'spacing' => 'one',
        ],
        'ternary_operator_spaces' => true,
        'ternary_to_null_coalescing' => true,
        'unary_operator_spaces' => true,
        'blank_line_after_opening_tag' => true,
        // php
        'full_opening_tag' => true,
        'no_closing_tag' => true,
        // return
        'no_useless_return' => true,
        'return_assignment' => true,
        'no_singleline_whitespace_before_semicolons' => true,
        'semicolon_after_instruction' => true,
        // string
        'explicit_string_variable' => true,
        'no_binary_string' => true,
        'simple_to_complex_string_variable' => true,
        'single_quote' => true,
        // whitespace
        'array_indentation' => true,
        'blank_line_before_statement' => [
            'statements' => [
                'exit',
                'for',
                'foreach',
                'if',
                'return',
                'switch',
                'throw',
                'try',
                'while',
            ],
        ],
        'compact_nullable_typehint' => true,
        'indentation_type' => true,
        'line_ending' => true,
        'method_chaining_indentation' => true,
        'no_extra_blank_lines' => true,
        'no_spaces_around_offset' => true,
        'no_spaces_inside_parenthesis' => true,
        'no_trailing_whitespace' => true,
        'no_whitespace_in_blank_line' => true,
        'single_blank_line_at_eof' => true,
    ])
    ->setFinder($finder);
