<?php

declare(strict_types=1);

namespace Tests\Database\Db;

use Exception;
use PHPUnit\Framework\TestCase;
use Tests\Database\Db\ToolkitTest\Connection;

class ToolkitTest extends TestCase
{
    private Connection $db;

    protected function setUp(): void
    {
        $this->db = new Connection();
    }

    /**
     * @throws Exception
     */
    public function testPrependPrefix(): void
    {
        $this->assertEquals(
            'SELECT customer_id FROM dkh_customer UNION SELECT value FROM `update`',
            $this->db->build('SELECT customer_id FROM customer UNION SELECT value FROM `update`'),
        );

        $this->assertEquals(
            'SELECT *, pd.description as product_description, dkh_promotion.description as promotion_description FROM dkh_product p LEFT JOIN dkh_product_description pd ON p.product_id = pd.product_id LEFT JOIN dkh_promotion ON p.product_id = dkh_promotion.product_id',
            $this->db->build('SELECT *, pd.description as product_description, promotion.description as promotion_description FROM product p LEFT JOIN product_description pd ON p.product_id = pd.product_id LEFT JOIN promotion ON p.product_id = promotion.product_id'),
        );
    }

    /**
     * @throws Exception
     */
    public function testEvaluateConstants(): void
    {
        $this->assertEquals(
            'SELECT customer_id FROM dkh_customer UNION SELECT value FROM `update`',
            $this->db->build('SELECT customer_id FROM customer UNION SELECT value FROM `update`'),
        );

        $this->assertEquals(
            'SELECT *, pd.description as product_description, dkh_promotion.description as promotion_description FROM dkh_product p LEFT JOIN dkh_product_description pd ON p.product_id = pd.product_id LEFT JOIN dkh_promotion ON p.product_id = dkh_promotion.product_id',
            $this->db->build('SELECT *, pd.description as product_description, promotion.description as promotion_description FROM product p LEFT JOIN product_description pd ON p.product_id = pd.product_id LEFT JOIN promotion ON p.product_id = promotion.product_id'),
        );
    }

    /**
     * @throws Exception
     */
    public function testBindParams(): void
    {
        $this->assertEquals(
            'SELECT * FROM dkh_customer WHERE customer_id = 1',
            $this->db->build('SELECT * FROM customer WHERE customer_id = ?i', [1]),
        );

        $this->assertEquals(
            'SELECT * FROM dkh_customer WHERE customer_id = 1',
            $this->db->build('SELECT * FROM customer WHERE customer_id = ?i', ['1 OR 1 --']),
            'no injection should be possible',
        );

        $this->assertEquals(
            "SELECT * FROM dkh_customer WHERE username = 'dkh'",
            $this->db->build('SELECT * FROM customer WHERE username = ?s', ['dkh']),
        );

        $this->assertEquals(
            "SELECT * FROM dkh_customer WHERE customer_id = 1 OR username = 'dkh'",
            $this->db->build(
                'SELECT * FROM customer WHERE customer_id = ?customer_id:i OR username = ?username:s',
                [
                    'customer_id' => 1,
                    'username' => 'dkh',
                ],
            ),
        );

        $this->assertEquals(
            "INSERT INTO dkh_customer_device (customer_id, ip) VALUES (1, '127.0.0.1')",
            $this->db->build(
                'INSERT INTO customer_device (customer_id, ip) VALUES (?customer_id:i, ?ip:s)',
                [
                    'customer_id' => 1,
                    'ip' => '127.0.0.1',
                ],
            ),
        );
    }

    /**
     * @throws Exception
     */
    public function testBindArrayParams(): void
    {
        $this->assertEquals(
            "SELECT * FROM dkh_customer WHERE customer_id IN (1,2) OR username IN ('dkh','framework','db')",
            $this->db->build(
                'SELECT * FROM customer WHERE customer_id IN ?customer_ids:[i] OR username IN ?usernames:[s]',
                [
                    'customer_ids' => [1, 2],
                    'usernames' => ['dkh', 'framework', 'db'],
                ],
            ),
        );
    }

    /**
     * @throws Exception
     */
    public function testBindEnumParams(): void
    {
        $this->assertEquals(
            'SELECT * FROM dkh_customer ORDER BY customer_id DESC',
            $this->db->build(
                'SELECT * FROM customer ORDER BY ?sort:sort_enum ?order:order_enum',
                [
                    'sort_enum' => ['customer_id', 'username'],
                    'order_enum' => ['ASC', 'DESC'],
                    'sort' => '-- INVALID',
                    'order' => 'DESC',
                ],
            ),
        );
    }

    /**
     * @throws Exception
     */
    public function testLikeCondition(): void
    {
        $this->assertEquals(
            "SELECT * FROM dkh_customers WHERE username LIKE '%dkh%'",
            $this->db->build(
                'SELECT * FROM customers WHERE username LIKE ?username_includes:%s%',
                [
                    'username_includes' => 'dkh',
                ],
            ),
        );
    }

    /**
     * @throws Exception
     */
    public function testConditionalFragment(): void
    {
        $this->assertEquals(
            'SELECT username, customer_id FROM dkh_customer WHERE customer_id <= 3',
            $this->db->build(
                '
                    SELECT
                        username, ?username:
                        balance, ?balance:
                        date_joined, ?date_joined:
                        customer_id
                    FROM customer
                    WHERE
                        AND customer_id >= ?customer_id_gte:i ?customer_id_gte:
                        AND customer_id <= ?customer_id_lte:i ?customer_id_lte:
                ',
                [
                    'username' => true,
                    'balance' => false,
                    'customer_id_lte' => 3,
                ],
            ),
        );

        $this->assertEquals(
            "INSERT INTO dkh_customer_device ( ip, customer_id ) VALUES ( '127.0.0.1', 1 )",
            $this->db->build(
                '
                    INSERT INTO dkh_customer_device (
                        user_agent, ?user_agent:
                        ip, ?ip:
                        customer_id
                    )
                    VALUES (
                        ?user_agent:s, ?user_agent:
                        ?ip:s, ?ip:
                        ?customer_id:i
                    )
                ',
                [
                    'customer_id' => 1,
                    'ip' => '127.0.0.1',
                ],
            ),
        );
    }

    /**
     * @throws Exception
     */
    public function testTrimConditionalFragment(): void
    {
        $this->assertEquals(
            'SELECT customer_id, username FROM dkh_customer WHERE customer_id <= 3',
            $this->db->build(
                '
                    SELECT
                        customer_id,
                        username, ?username:
                        balance, ?balance:
                        date_joined, ?date_joined:
                    FROM customer
                    WHERE
                        AND customer_id >= ?customer_id_gte:i ?customer_id_gte:
                        AND customer_id <= ?customer_id_lte:i ?customer_id_lte:
                ',
                [
                    'username' => true,
                    'balance' => false,
                    'customer_id_lte' => 3,
                ],
            ),
        );

        $this->assertEquals(
            "INSERT INTO dkh_customer_device ( customer_id, ip ) VALUES ( 1, '127.0.0.1' )",
            $this->db->build(
                '
                    INSERT INTO dkh_customer_device (
                        customer_id,
                        user_agent, ?user_agent:
                        ip, ?ip:
                    )
                    VALUES (
                        ?customer_id:i,
                        ?user_agent:s, ?user_agent:
                        ?ip:s, ?ip:
                    )
                ',
                [
                    'customer_id' => 1,
                    'ip' => '127.0.0.1',
                ],
            ),
        );

        $this->assertEquals(
            "SELECT cart_id, payload FROM dkh_cart WHERE session_id = '123456789' AND ( customer_id = 0 OR customer_id = 1 )",
            $this->db->build(
                '
                    SELECT
                        cart_id,
                        product_id, ?product_id:
                        payload, ?payload:
                    FROM cart
                    WHERE
                        AND session_id = ?session_id:s
                        AND (
                            OR customer_id = 0
                            OR customer_id = 1
                        )
                ',
                [
                    'payload' => true,
                    'session_id' => '123456789',
                    'customer_id' => 1,
                ],
            ),
        );
    }

    /**
     * @throws Exception
     */
    public function testConditionalFragmentThrowException(): void
    {
        $this->expectException(Exception::class);

        $this->assertEquals(
            'SELECT username, customer_id FROM dkh_customer WHERE customer_id <= 3',
            $this->db->build(
                '
                    SELECT
                        username, ?username:
                        balance, ?balance:
                        date_joined, ?date_joined:
                        customer_id
                    FROM customer
                    WHERE
                        AND customer_id >= ?customer_id_gte:i
                        AND customer_id <= ?customer_id_lte:i
                ',
                [
                    'username' => true,
                    'balance' => false,
                    'customer_id_lte' => 3,
                ],
            ),
        );
    }

    /**
     * @throws Exception
     */
    public function testSpreadValues(): void
    {
        $this->assertEquals(
            "INSERT INTO dkh_customer_device (customer_id, ip) VALUES (1, '127.0.0.1'), (2, '1.0.0.0')",
            $this->db->build(
                'INSERT INTO customer_device (customer_id, ip) VALUES ...(?customer_id:i, ?ip:s)',
                [
                    [
                        'customer_id' => 1,
                        'ip' => '127.0.0.1',
                    ],
                    [
                        'customer_id' => 2,
                        'ip' => '1.0.0.0',
                    ],
                ],
            ),
        );

        $this->assertEquals(
            "INSERT INTO dkh_customer_device ( ip, customer_id ) VALUES ( '127.0.0.1', 1 ), ( '1.0.0.0', 2 )",
            $this->db->build(
                '
                    INSERT INTO dkh_customer_device (
                        user_agent, ?user_agent:
                        ip, ?ip:
                        customer_id
                    )
                    VALUES ...(
                        ?user_agent:s, ?user_agent:
                        ?ip:s,
                        ?customer_id:i
                    )
                ',
                [
                    [
                        'customer_id' => 1,
                        'ip' => '127.0.0.1',
                    ],
                    [
                        'customer_id' => 2,
                        'ip' => '1.0.0.0',
                    ],
                ],
            ),
        );

        $this->assertEquals(
            "INSERT INTO dkh_customer_device ( customer_id, ip ) VALUES ( 1, '127.0.0.1' ), ( 2, '1.0.0.0' ) ON DUPLICATE KEY UPDATE ip = VALUES(ip)",
            $this->db->build(
                '
                    INSERT INTO dkh_customer_device (
                        customer_id,
                        user_agent, ?user_agent:
                        ip, ?ip:
                    )
                    VALUES ...(
                        ?customer_id:i,
                        ?ip:s,
                        ?user_agent:s, ?user_agent:
                    )
                    ON DUPLICATE KEY UPDATE
                        ip = VALUES(ip)
                ',
                [
                    [
                        'customer_id' => 1,
                        'ip' => '127.0.0.1',
                    ],
                    [
                        'customer_id' => 2,
                        'ip' => '1.0.0.0',
                    ],
                ],
            ),
        );

        $this->assertEquals(
            "INSERT INTO dkh_customer_device ( ip, customer_id ) VALUES ( '127.0.0.1', 1 ), ( '1.0.0.0', 2 ) ON DUPLICATE KEY UPDATE ip = VALUES(ip)",
            $this->db->build(
                '
                    INSERT INTO dkh_customer_device (
                        user_agent, ?user_agent:
                        ip, ?ip:
                        customer_id
                    )
                    VALUES ...(
                        ?user_agent:s, ?user_agent:
                        ?ip:s,
                        ?customer_id:i
                    )
                    ON DUPLICATE KEY UPDATE
                        user_agent = VALUES(user_agent), ?user_agent:
                        ip = VALUES(ip),
                ',
                [
                    [
                        'customer_id' => 1,
                        'ip' => '127.0.0.1',
                    ],
                    [
                        'customer_id' => 2,
                        'ip' => '1.0.0.0',
                    ],
                ],
            ),
        );

        $this->assertEquals(
            "UPDATE dkh_customer_device cd JOIN (SELECT 1 AS customer_id, '127.0.0.1' AS ip UNION ALL SELECT 2 AS customer_id, '1.0.0.0' AS ip) v ON cd.customer_id = v.customer_id SET cd.ip = v.ip",
            $this->db->build(
                '
                    UPDATE dkh_customer_device cd
                    JOIN
                        ...(SELECT ?customer_id:i AS customer_id, ?ip:s AS ip) v
                        ON cd.customer_id = v.customer_id
                    SET cd.ip = v.ip
                ',
                [
                    [
                        'customer_id' => 1,
                        'ip' => '127.0.0.1',
                    ],
                    [
                        'customer_id' => 2,
                        'ip' => '1.0.0.0',
                    ],
                ],
            ),
        );
    }

    public function testGetLastIds(): void
    {
        $ids = $this->db->getLastIds();

        $this->assertEquals($this->db->getLastId(), $ids[0]);
        $this->assertCount($this->db->countAffected(), $ids);
    }

    public function testThrowException(): void
    {
        $this->expectException(Exception::class);

        $this->db->build(
            'SELECT * FROM customer WHERE customer_id IN ?[i]',
            [1],
        );
    }
}
