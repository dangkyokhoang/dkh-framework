<?php

declare(strict_types=1);

namespace Tests\Database\Db\ToolkitTest;

use Dkh\Database\Db\Toolkit;

class Connection
{
    use Toolkit;

    private string $prefix = 'dkh_';

    public function getLastId(): int
    {
        return 7;
    }

    public function countAffected(): int
    {
        return 3;
    }

    public function escape(string $value): string
    {
        return str_replace(
            ['\\',  "\x00", "\n",  "\r",  "'",  '"', "\x1a"],
            ['\\\\', '\\0', '\\n', '\\r', "\'", '\"', '\\Z'],
            $value,
        );
    }
}
