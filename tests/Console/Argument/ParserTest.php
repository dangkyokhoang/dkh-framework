<?php

declare(strict_types=1);

namespace Tests\Console\Argument;

use Dkh\Console\Argument\Parser;
use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase
{
    public function test(): void
    {
        $this->assertEquals(
            [
                // indexed
                'e',
                'l/',
                // named
                'a' => 'b',
                'b' => true,
                'c' => 'd',
                'e' => true,
                'f' => '"g h"',
                'i' => 123,
                'j' => '/k',
                'k' => false,
            ],
            Parser::parse(['--a=b', '-b', '-c', 'd', 'e', '--e', '--f="g h"', '-i=123', '-j=/k', 'l/', '-k', 'false']),
        );
    }
}
