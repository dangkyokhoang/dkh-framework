<?php

declare(strict_types=1);

namespace Tests\Console\Scheduler;

use Dkh\Console\Scheduler\Parser;
use Exception;
use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function test(): void
    {
        $this->assertEquals(
            [
                [
                    [
                        null,
                        null,
                        null,
                        null,
                        null,
                    ],
                    [
                        [Parser::class, 'parse'],
                    ],
                ],
                [
                    [
                        [0, 5, 10, 15, 20, 25, 30, 40, 50],
                        null,
                        null,
                        null,
                        // 0 - Sunday is converted to 7
                        [7, 3, 6],
                    ],
                    [
                        [Parser::class, 'parse'],
                        [Parser::class, 'parse'],
                    ],
                ],
            ],
            Parser::parse([
                '* * * * *' => [Parser::class, 'parse'],
                '0-29/5,30/10 * * * 0,3,6' => [
                    [Parser::class, 'parse'],
                    [Parser::class, 'parse'],
                ],
            ]),
        );
    }
}
