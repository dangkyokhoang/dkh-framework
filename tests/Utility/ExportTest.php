<?php

declare(strict_types=1);

namespace Tests\Utility;

use Dkh\Utility\Export;
use PHPUnit\Framework\TestCase;

class ExportTest extends TestCase
{
    public function test(): void
    {
        $this->assertEquals("[\n  1,\n]", Export::variable([1]));

        $this->assertEquals(
            "[\n  1,\n  [\n    'value' => 2,\n  ],\n]",
            Export::variable([1, ['value' => 2]]),
        );

        // escape characters
        $this->assertEquals(
            <<<'STRING'
            '\'"
            \\\\\'"'
            STRING,
            Export::variable("'\"\n\\\'\""),
        );
    }
}
