<?php

declare(strict_types=1);

namespace Tests\Utility;

use Dkh\Utility\Accessor;
use PHPUnit\Framework\TestCase;

class AccessorTest extends TestCase
{
    public function test(): void
    {
        // set top-level config
        Accessor::replace([
            'db' => [
                'database' => 'divineshop_db',
            ],
        ]);

        // set nested config
        Accessor::set('db.hostname', 'localhost');
        Accessor::set('db.options.charset', 'utf8');

        $this->assertEquals(
            [
                'database' => 'divineshop_db',
                'hostname' => 'localhost',
                'options' => [
                    'charset' => 'utf8',
                ],
            ],
            Accessor::get('db'),
            'get top-level config',
        );
        $this->assertEquals('divineshop_db', Accessor::get('db.database'), 'get nested config');
        $this->assertEquals('utf8', Accessor::get('db.options.charset'), 'get nested config');

        // replace the current config with this config
        Accessor::replace([
            'db' => [
                'hostname' => '192.168.104.100',
                'username' => 'root',
                'password' => '',
                'database' => 'divine_study_db',
                'port' => 3306,
                'prefix' => 'dv_',
            ],
        ]);

        $this->assertEquals(3306, Accessor::get('db.port'), 'updated config');

        Accessor::unset('db.port');

        $this->assertEquals(null, Accessor::get('db.port'), 'already unset');
        $this->assertEquals(null, Accessor::get('not'), 'doest not exist');
        $this->assertEquals(null, Accessor::get('not.exists'), 'doest not exist');
    }
}
