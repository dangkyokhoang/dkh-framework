<?php

declare(strict_types=1);

namespace Tests\Validation;

use Dkh\Error\FormError;
use Dkh\Validation\Validator;
use PHPUnit\Framework\TestCase;

class ValidatorTest extends TestCase
{
    public const DATA = [
        'email' => 'person@example.com',
        'password' => '123456',
    ];

    /**
     * @throws FormError
     */
    public function testConstraint(): void
    {
        Validator::constraint(
            static::DATA,
            [
                'email' => [
                    'required' => true,
                    'type' => 'email',
                ],
                'password' => [
                    'required' => true,
                    'minlength' => 6,
                ],
            ],
        );

        try {
            Validator::constraint(
                static::DATA,
                [
                    'email' => [
                        'required' => true,
                        'type' => 'email',
                    ],
                    'username' => [
                        'required' => true,
                    ],
                    'password' => [
                        'required' => true,
                        'minlength' => 8,
                    ],
                ],
            );

            $this->fail('invalid field password');
        } catch (FormError $error) {
            $this->assertEquals(
                [
                    'username' => FormError::VALUE_MISSING,
                    'password' => FormError::TOO_SHORT,
                ],
                $error->getPayload(),
            );
        }

        try {
            Validator::constraint(
                static::DATA,
                [
                    'email' => [
                        'required' => true,
                        'type' => 'email',
                    ],
                    'password' => [
                        'required' => true,
                        'minlength' => 8,
                    ],
                ],
            );

            $this->fail('invalid field password');
        } catch (FormError $error) {
            $this->assertEquals(
                [
                    'password' => FormError::TOO_SHORT,
                ],
                $error->getPayload(),
            );
        }
    }

    /**
     * @throws FormError
     */
    public function testStrictConstraint(): void
    {
        Validator::constraint(
            static::DATA,
            [
                'email' => [
                    'required' => true,
                    'type' => 'email',
                ],
            ],
            false,
        );

        try {
            Validator::constraint(
                static::DATA,
                [
                    'email' => [
                        'required' => true,
                        'type' => 'email',
                    ],
                ],
            );

            $this->fail('invalid field password');
        } catch (FormError $error) {
            $this->assertEquals(
                [
                    'password' => FormError::BAD_INPUT,
                ],
                $error->getPayload(),
            );
        }
    }
}
