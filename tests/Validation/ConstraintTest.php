<?php

declare(strict_types=1);

namespace Tests\Validation;

use Dkh\Error\FormError;
use Dkh\Validation\Constraint;
use Exception;
use PHPUnit\Framework\TestCase;

class ConstraintTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testTypes(): void
    {
        $this->assertTrue(Constraint::type('email', 'person@example.com'));
        $this->assertTrue(Constraint::type('email', 'per-son@example.com'));
        $this->assertFalse(Constraint::type('email', 'person at example.com'));

        $this->assertTrue(Constraint::type('url', 'https://example.com/'));
        $this->assertTrue(Constraint::type('url', 'https://example.com/test'));
        $this->assertFalse(Constraint::type('url', 'example.com'));

        $this->assertTrue(Constraint::type('number', '15'));
        $this->assertTrue(Constraint::type('number', '-015'));
        $this->assertTrue(Constraint::type('number', '0.15'));
        $this->assertTrue(Constraint::type('number', '.15'));
        $this->assertFalse(Constraint::type('number', '15.'));
        $this->assertFalse(Constraint::type('number', '1e8'));

        $this->assertTrue(Constraint::type('tel', '0123456789'));
        $this->assertTrue(Constraint::type('tel', '+84123456789'));
        $this->assertTrue(Constraint::type('tel', '+1123456789'));
        $this->assertTrue(Constraint::type('tel', '1123456789'));

        $this->assertTrue(Constraint::type('date', '2021-01-01'));
        $this->assertFalse(Constraint::type('date', '2021/01/01'));

        $this->assertTrue(Constraint::type('time', '00:00:30.75'));
        $this->assertTrue(Constraint::type('time', '12:15'));
        $this->assertTrue(Constraint::type('time', '13:44:25'));
        $this->assertFalse(Constraint::type('time', '13.44.25'));

        $this->assertTrue(Constraint::type('datetime', '2021-01-01T00:00:30.75'));
        $this->assertTrue(Constraint::type('datetime', '2021-01-01T12:15'));
        $this->assertTrue(Constraint::type('datetime', '2021-01-01T13:44:25'));
        $this->assertTrue(Constraint::type('datetime', '2021-01-01T13:44:25'));

        $this->assertTrue(Constraint::type('password', 'whatever'));
    }

    /**
     * @throws Exception
     */
    public function testValidate(): void
    {
        $this->assertEquals(
            FormError::VALUE_MISSING,
            Constraint::validate(['required' => true, 'min' => 10], null),
        );

        $this->assertEquals(
            null,
            Constraint::validate(['min' => 10], null),
        );

        $this->assertEquals(
            null,
            Constraint::validate(['required' => true, 'min' => 10], 10),
        );

        $this->assertEquals(
            null,
            Constraint::validate(['min' => 10], 10),
        );

        $this->assertEquals(
            null,
            Constraint::validate(['required' => true, 'min' => 10], 11),
        );

        $this->assertEquals(
            FormError::RANGE_UNDERFLOW,
            Constraint::validate(['required' => true, 'min' => 10], 9),
        );

        $this->assertEquals(
            null,
            Constraint::validate(['max' => 10], 10),
        );

        $this->assertEquals(
            FormError::RANGE_OVERFLOW,
            Constraint::validate(['max' => 10], 11),
        );

        $this->assertEquals(
            null,
            Constraint::validate(['step' => 10], 20),
        );

        $this->assertEquals(
            FormError::STEP_MISMATCH,
            Constraint::validate(['step' => 10], 19),
        );

        $this->assertEquals(
            FormError::STEP_MISMATCH,
            Constraint::validate(['min' => 9, 'step' => 10], 20),
        );

        $this->assertEquals(
            null,
            Constraint::validate(['min' => 9, 'step' => 10], 19),
        );

        $this->assertEquals(
            null,
            Constraint::validate(['maxlength' => 3], 'one'),
        );

        $this->assertEquals(
            FormError::TOO_LONG,
            Constraint::validate(['maxlength' => 3], 'three'),
        );

        $this->assertEquals(
            FormError::TOO_SHORT,
            Constraint::validate(['minlength' => 4], 'one'),
        );

        $this->assertEquals(
            null,
            Constraint::validate(['minlength' => 4], 'three'),
        );

        $this->assertEquals(
            null,
            Constraint::validate(['minlength' => 4], ''),
        );

        $this->assertEquals(
            FormError::VALUE_MISSING,
            Constraint::validate(['required' => true, 'minlength' => 4], ''),
        );

        $this->assertEquals(
            null,
            Constraint::validate(['type' => 'email'], 'person@example.com'),
        );

        $this->assertEquals(
            FormError::TYPE_MISMATCH,
            Constraint::validate(['type' => 'email'], 'example.com'),
        );

        $this->assertEquals(
            null,
            Constraint::validate(['pattern' => '^\d+$'], '1'),
        );

        $this->assertEquals(
            FormError::PATTERN_MISMATCH,
            Constraint::validate(['pattern' => '^\d+$'], '1.25'),
        );
    }
}
