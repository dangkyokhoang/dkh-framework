<?php

namespace Tests\Schema\FormatterTest;

class Node
{
    public int $data;
    public ?Node $next;
}
