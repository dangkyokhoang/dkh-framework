<?php

declare(strict_types=1);

namespace Tests\Schema\FormatterTest;

use Dkh\Schema\Parser\ArrayType;

class Result
{
    #[ArrayType('int')]
    public ?array $numbers;
}
