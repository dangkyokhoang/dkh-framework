<?php

namespace Tests\Schema\FormatterTest;

use Dkh\Schema\Parser\ArrayType;

class Comment
{
    public string $text;
    #[ArrayType(Comment::class)]
    public ?array $replies;
}
