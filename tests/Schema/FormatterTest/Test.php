<?php

declare(strict_types=1);

namespace Tests\Schema\FormatterTest;

use Dkh\Schema\Parser\Alias;

class Test
{
    #[Alias('test_id')]
    public int $id;
    public Result $result;
    public ?int $serviceId = 1;
}
