<?php

declare(strict_types=1);

namespace Tests\Schema\FormatterTest;

use Dkh\Schema\Parser\ArrayType;

class Report
{
    #[ArrayType(Test::class)]
    public null|array $tests;
}
