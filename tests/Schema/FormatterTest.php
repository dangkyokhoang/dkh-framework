<?php

declare(strict_types=1);

namespace Tests\Schema;

use Dkh\Schema\Formatter;
use Dkh\Schema\Parser;
use Exception;
use PHPUnit\Framework\TestCase;
use Tests\Schema\FormatterTest\Comment;
use Tests\Schema\FormatterTest\Node;
use Tests\Schema\FormatterTest\Report;

class FormatterTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testFormat(): void
    {
        $this->assertEquals(
            [
                'tests' => [
                    [
                        'id' => 1,
                        'result' => [
                            'numbers' => [1, 2],
                        ],
                        'serviceId' => 5,
                    ],
                    [
                        'id' => 2,
                        'result' => [],
                    ],
                ],
            ],
            Formatter::format(
                Parser::parse(Report::class),
                [
                    'tests' => [
                        [
                            'test_id' => '1',
                            'result' => [
                                'numbers' => ['1', '2'],
                            ],
                            'service_id' => '5',
                        ],
                        [
                            'test_id' => '2',
                            'result' => [],
                        ],
                    ],
                ],
            ),
        );
    }

    /**
     * @throws Exception
     */
    public function testFormatDefaultValue(): void
    {
        $this->assertEquals(
            [
                'tests' => [
                    [
                        'id' => 1,
                        'result' => [
                            'numbers' => [1, 2],
                        ],
                    ],
                    [
                        'id' => 2,
                        'result' => [],
                    ],
                ],
            ],
            Formatter::format(
                Parser::parse(Report::class),
                [
                    'tests' => [
                        [
                            'test_id' => '1',
                            'result' => [
                                'numbers' => ['1', '2'],
                            ],
                            'service_id' => '1',
                        ],
                        [
                            'test_id' => '2',
                            'result' => [],
                        ],
                    ],
                ],
            ),
        );
    }

    /**
     * @throws Exception
     */
    public function testFormatRecursiveSchema(): void
    {
        //        $this->assertEquals(
        //            [
        //                'data' => 1,
        //                'next' => [
        //                    'data' => 3,
        //                    'next' => [
        //                        'data' => 2,
        //                    ],
        //                ],
        //            ],
        //            Formatter::format(Node::class, [
        //                'data' => '1',
        //                'next' => [
        //                    'data' => '3',
        //                    'next' => [
        //                        'data' => '2',
        //                        'next' => null,
        //                    ],
        //                ],
        //            ]),
        //        );

        $this->assertEquals(
            [
                'text' => '1',
                'replies' => [
                    [
                        'text' => '2',
                    ],
                    [
                        'text' => '3',
                        'replies' => [
                            [
                                'text' => '4',
                            ],
                            [
                                'text' => '5',
                            ],
                        ],
                    ],
                ],
            ],
            Formatter::format(Comment::class, [
                'text' => 1,
                'replies' => [
                    [
                        'text' => 2,
                    ],
                    [
                        'text' => 3,
                        'replies' => [
                            [
                                'text' => 4,
                            ],
                            [
                                'text' => 5,
                            ],
                        ],
                    ],
                ],
            ]),
        );
    }

    public function testFormatException(): void
    {
        try {
            Formatter::format(
                Parser::parse(Report::class),
                [
                    'tests' => [
                        [
                            'test_id' => '1',
                            'result' => [
                                'numbers' => ['1', '2'],
                            ],
                        ],
                        [
                            'test_id' => '2',
                            'result' => [
                                'numbers' => '3',
                            ],
                        ],
                    ],
                ],
            );
        } catch (Exception $exception) {
            $messages = [];

            do {
                $messages[] = $exception->getMessage();
                $exception = $exception->getPrevious();
            } while ($exception);

            $messages = implode("\n", $messages);

            $this->assertStringContainsString('tests', $messages);
            $this->assertStringContainsString('1', $messages);
            $this->assertStringContainsString('result', $messages);
            $this->assertStringContainsString('numbers', $messages);
        }
    }
}
