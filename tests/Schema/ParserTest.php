<?php

declare(strict_types=1);

namespace Tests\Schema;

use Dkh\Schema\Parser;
use Dkh\Schema\Type;
use Exception;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use Tests\Schema\FormatterTest\Comment;
use Tests\Schema\FormatterTest\Node;
use Tests\Schema\FormatterTest\Report;
use Tests\Schema\FormatterTest\Result;
use Tests\Schema\FormatterTest\Test;

class ParserTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function test(): void
    {
        $this->assertEquals(
            [
                'type' => Type::OBJECT,
                'properties' => [
                    'numbers' => [
                        'type' => Type::ARRAY,
                        'items' => [
                            'type' => Type::INT,
                            'nullable' => false,
                        ],
                        'nullable' => true,
                    ],
                ],
            ],
            Parser::parse(Result::class),
        );

        $this->assertEquals(
            [
                'type' => Type::OBJECT,
                'properties' => [
                    'id' => [
                        'type' => Type::INT,
                        'nullable' => false,
                        'key' => 'test_id',
                    ],
                    'result' => [
                        'type' => Type::OBJECT,
                        'properties' => [
                            'numbers' => [
                                'type' => Type::ARRAY,
                                'items' => [
                                    'type' => Type::INT,
                                    'nullable' => false,
                                ],
                                'nullable' => true,
                            ],
                        ],
                        'nullable' => false,
                    ],
                    'serviceId' => [
                        'type' => Type::INT,
                        'nullable' => true,
                        'default' => 1,
                        'key' => 'service_id',
                    ],
                ],
            ],
            Parser::parse(Test::class),
        );

        $this->assertEquals(
            [
                'type' => Type::OBJECT,
                'properties' => [
                    'tests' => [
                        'type' => Type::ARRAY,
                        'items' => [
                            'type' => Type::OBJECT,
                            'properties' => [
                                'id' => [
                                    'type' => Type::INT,
                                    'nullable' => false,
                                    'key' => 'test_id',
                                ],
                                'result' => [
                                    'type' => Type::OBJECT,
                                    'properties' => [
                                        'numbers' => [
                                            'type' => Type::ARRAY,
                                            'items' => [
                                                'type' => Type::INT,
                                                'nullable' => false,
                                            ],
                                            'nullable' => true,
                                        ],
                                    ],
                                    'nullable' => false,
                                ],
                                'serviceId' => [
                                    'type' => Type::INT,
                                    'nullable' => true,
                                    'default' => 1,
                                    'key' => 'service_id',
                                ],
                            ],
                            'nullable' => false,
                        ],
                        'nullable' => true,
                    ],
                ],
            ],
            Parser::parse(Report::class),
        );
    }

    /**
     * @throws ReflectionException
     */
    public function testRecursiveSchema(): void
    {
        $this->assertEquals(
            [
                'type' => Type::OBJECT,
                'properties' => [
                    'data' => [
                        'type' => Type::INT,
                        'nullable' => false,
                    ],
                    'next' => [
                        'type' => Node::class,
                        'nullable' => true,
                    ],
                ],
            ],
            Parser::parse(Node::class),
        );

        $this->assertEquals(
            [
                'type' => Type::OBJECT,
                'properties' => [
                    'text' => [
                        'type' => Type::STRING,
                        'nullable' => false,
                    ],
                    'replies' => [
                        'type' => Type::ARRAY,
                        'items' => [
                            'type' => Comment::class,
                            'nullable' => false,
                        ],
                        'nullable' => true,
                    ],
                ],
            ],
            Parser::parse(Comment::class),
        );
    }
}
