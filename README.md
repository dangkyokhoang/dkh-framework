# `Dkh\Framework`

_A minimal framework for building high performance APIs_

## Example

`src/index.php`
``` php
<?php

namespace App;

use Dkh\Error\ClientError;
use Dkh\Framework;
use Dkh\Framework\Configuration;
use Dkh\Framework\Handler;
use Dkh\Framework\Middleware;
use Dkh\Http\Response;

Configuration::replace(require __DIR__ . '/configuration.php');

Middleware::set(require __DIR__ . '/middlewares.php');

Handler::set(require __DIR__ . '/routes.php');

try {
    Framework::start();
} catch (ClientError $error) {
    Response::status($error->getStatus());
    Response::end($error);
}
```

`src/routes.php`
``` php
<?php

use App\Controller\Product;
use App\Controller\Cart;

return [
  'GET' => [
    '/api/product/list' => [Product::class, 'list'],
  ],
  'POST' => [
    '/api/cart/add' => [Cart::class, 'add'],
  ],
];
```

`src/middlewares.php`
``` php
<?php

use App\Middleware\Http;
use App\Middleware\Authentication;

return [
    '/' => [Http::class, 'cors'],
    '/api/cart' => [
        [Http::class, 'session'],
        [Authentication::class, 'user'],
    ],
];
```

`configuration.php`
``` php
<?php

return [
    'db' => [
        [
            'hostname' => '127.0.0.1',
            'username' => 'root',
            'password' => '',
            'database' => 'dkh_db',
            'port' => 3306,
            'prefix' => 'dkh_',
        ],
        [
            'hostname' => '127.0.0.2',
            'database' => 'dv_db',
            'prefix' => 'dv_',
        ],
    ],
    'redisx' => [
        'host' => '127.0.0.1',
        'port' => 6379,
        'prefix' => 'x_',
    ],
    'schema' => [
        'root' => __DIR__ . '/local/schema',
    ],
];
```
