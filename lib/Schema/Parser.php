<?php

declare(strict_types=1);

namespace Dkh\Schema;

use Dkh\Schema\Parser\Alias;
use Dkh\Schema\Parser\ArrayType;
use Exception;
use JetBrains\PhpStorm\ArrayShape;
use ReflectionClass;
use ReflectionException;
use ReflectionNamedType;
use ReflectionProperty;
use ReflectionUnionType;
use Throwable;

class Parser
{
    public function __construct(private string $class)
    {
    }

    /**
     * @return array
     * @throws ReflectionException
     * @throws Exception
     */
    #[ArrayShape(['type' => 'int', 'properties' => 'array'])]
    public function run(): array
    {
        $properties = (new ReflectionClass($this->class))->getProperties(
            ReflectionProperty::IS_PUBLIC,
        );
        $properties = array_map(
            function (ReflectionProperty $property) {
                try {
                    return $this->parseProperty($property);
                } catch (Throwable $error) {
                    throw new Exception((string)$property, 0, $error);
                }
            },
            $properties,
        );

        return [
            'type' => Type::OBJECT,
            'properties' => array_column($properties, 1, 0),
        ];
    }

    /**
     * @param ReflectionProperty $property
     * @return array
     * @throws Exception
     */
    private function parseProperty(ReflectionProperty $property): array
    {
        $type = $property->getType();

        if (!$type) {
            throw new Exception('type must be specified');
        }

        $nullable = $type->allowsNull();

        $type = $this->parseType($type);

        if ($property->hasDefaultValue()) {
            $type['nullable'] = true;
            $type['default'] = $property->getDefaultValue();
        } else {
            $type['nullable'] = $nullable;
        }

        // array item type

        $items = $property->getAttributes(ArrayType::class);

        if ($items) {
            [$name] = $items[0]->getArguments();

            $items = $this->resolveType($name);
            $items['nullable'] = false;

            $type['items'] = $items;
        }

        // property name and alias

        $name = $property->getName();
        $alias = $property->getAttributes(Alias::class);

        if ($alias) {
            [$alias] = $alias[0]->getArguments();
            $type['key'] = $alias;
        } else {
            $alias = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $name));

            if ($alias !== $name) {
                $type['key'] = $alias;
            }
        }

        return [$name, $type];
    }

    /**
     * @throws Exception
     */
    public function parseType(ReflectionNamedType|ReflectionUnionType $type): array
    {
        if ($type instanceof ReflectionNamedType) {
            $name = $type->getName();
        } else {
            $types = $type->getTypes();

            if (count($types) !== 2) {
                throw new Exception('mixed type is not allowed');
            }

            $name = $types[0]->getName();
            $name = $name !== 'null' ? $name : $types[1]->getName();
        }

        return $this->resolveType($name);
    }

    /**
     * @param string $name
     * @return array
     * @throws ReflectionException
     */
    private function resolveType(string $name): array
    {
        if ($name === $this->class) {
            return ['type' => $name];
        }

        $type = Type::get($name);

        if ($type) {
            return ['type' => $type];
        }

        return static::parse($name);
    }

    /**
     * @param string $class
     * @return array
     * @throws ReflectionException
     */
    #[ArrayShape(['type' => 'int', 'properties' => 'array'])]
    public static function parse(string $class): array
    {
        return (new static($class))->run();
    }
}
