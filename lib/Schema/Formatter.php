<?php

declare(strict_types=1);

namespace Dkh\Schema;

use Dkh\Utility\Export;
use Exception;
use Throwable;

class Formatter
{
    /**
     * @param mixed $schema
     * @param mixed $value
     * @param bool $nullable
     * @return string|array|bool|int|float
     * @throws Exception
     */
    public static function format(mixed $schema, mixed $value, bool $nullable = false): string|array|bool|int|float
    {
        if ($value === null && !$nullable) {
            throw new Exception('value must not be null');
        }

        if (is_string($schema)) {
            $schema = Loader::get($schema);
        }

        $type = is_int($schema) ? $schema : $schema['type'];

        Type::validate($type, $value);

        switch ($type) {
            case Type::STRING:
                return (string)$value;
            case Type::INT:
                return (int)$value;
            case Type::FLOAT:
                return (float)$value;
            case Type::BOOL:
                return (bool)$value;
            case Type::ARRAY:
                return static::array($schema['items'], $value);
            case Type::OBJECT:
                return static::object($schema, $value);
            case Type::ENUM:
                $values = $schema['values'];

                if (!in_array($value, $values)) {
                    throw new Exception('invalid enum value ' . Export::variable($value));
                }

                return $value;
            default:
                throw new Exception('unknown type ' . Export::variable($type));
        }
    }

    /**
     * @param array $schema
     * @param array $array
     * @return array
     * @throws Exception
     */
    private static function array(array $schema, array $array): array
    {
        $result = [];

        $type = is_int($schema['type']) ? $schema : $schema['type'];

        foreach ($array as $key => $item) {
            if ($item === null) {
                throw new Exception('array item ' . $key . ' must not be null');
            }

            try {
                $result[] = static::format($type, $item);
            } catch (Throwable $error) {
                throw new Exception('array item ' . $key, 0, $error);
            }
        }

        return $result;
    }

    /**
     * @param array $schema
     * @param array $object
     * @return array
     * @throws Exception
     */
    private static function object(array $schema, array $object): array
    {
        $result = [];

        foreach ($schema['properties'] as $property => $schema) {
            $key = $schema['key'] ?? $property;
            $nullable = $schema['nullable'];
            $value = $object[$key] ?? null;

            if ($value === null) {
                if ($nullable) {
                    continue;
                }

                throw new Exception('property ' . $key . ' must not be null');
            }

            $type = is_int($schema['type']) ? $schema : $schema['type'];
            $default = $schema['default'] ?? null;

            try {
                $value = static::format($type, $value);

                if ($nullable && $value === $default) {
                    continue;
                }

                $result[$property] = $value;
            } catch (Throwable $error) {
                throw new Exception('property ' . $key, 0, $error);
            }
        }

        return $result;
    }
}
