<?php

declare(strict_types=1);

namespace Dkh\Schema;

use Dkh\Framework\Configuration;
use Exception;

class Loader
{
    private static array $cache = [];

    /**
     * @param string $type
     * @return mixed
     * @throws Exception
     */
    public static function get(string $type): mixed
    {
        if (!array_key_exists($type, static::$cache)) {
            static::$cache[$type] = static::load($type);
        }

        return static::$cache[$type];
    }

    /**
     * @param string $type
     * @return array
     * @throws Exception
     */
    public static function load(string $type): array
    {
        $file = Configuration::get('schema.root') . '/' . $type . '.php';

        // schema definitions might be transpiled into schema array
        // in such case, require() is used to get the schema definition
        if (is_file($file)) {
            return require $file;
        }

        return Parser::parse($type);
    }
}
