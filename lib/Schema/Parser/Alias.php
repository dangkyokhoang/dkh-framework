<?php

declare(strict_types=1);

namespace Dkh\Schema\Parser;

use Attribute;

#[Attribute(Attribute::TARGET_PROPERTY)]
class Alias
{
    public function __construct(string $key)
    {
    }
}
