<?php

declare(strict_types=1);

namespace Dkh\Schema;

use Dkh\Utility\Export;
use Exception;

class Type
{
    public const STRING = 1;
    public const INT = 2;
    public const FLOAT = 3;
    public const BOOL = 4;
    public const ARRAY = 5;
    public const OBJECT = 6;
    public const ENUM = 7;

    private const MAP = [
        'string' => self::STRING,
        'int' => self::INT,
        'float' => self::FLOAT,
        'bool' => self:: BOOL,
        'array' => self::ARRAY,
    ];

    private const SCALAR = [
        'string' => self::STRING,
        'int' => self::INT,
        'float' => self::FLOAT,
        'bool' => self:: BOOL,
    ];

    public static function get(string $type): ?int
    {
        return self::MAP[$type] ?? null;
    }

    /**
     * @param int $type
     * @param mixed $value
     * @throws Exception
     */
    public static function validate(int $type, mixed $value): void
    {
        if (in_array($type, self::SCALAR) && !is_scalar($value)) {
            throw new Exception('value must be scalar ' . Export::variable($value));
        }
    }
}
