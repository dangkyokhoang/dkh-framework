<?php

declare(strict_types=1);

namespace Dkh\Utility;

abstract class Accessor
{
    protected static array $data;

    /**
     * @param string|null $name
     * @return mixed
     */
    public static function get(string $name = null): mixed
    {
        if ($name === null) {
            return static::$data;
        }

        $keys = explode('.', $name);

        $data = static::$data;

        foreach ($keys as $key) {
            $data = $data[$key] ?? null;
        }

        return $data;
    }

    public static function set(string $name, $value = null): void
    {
        $keys = explode('.', $name);

        $data = &static::$data;

        foreach ($keys as $key) {
            $data = &$data[$key];
        }

        $data = $value;

        unset($data);
    }

    public static function unset(string $name): void
    {
        $keys = explode('.', $name);
        $last = array_pop($keys);

        $data = &static::$data;

        foreach ($keys as $key) {
            $data = &$data[$key];
        }

        unset($data[$last]);
    }

    public static function replace(array $data): void
    {
        static::$data = $data;
    }
}
