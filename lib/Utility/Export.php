<?php

declare(strict_types=1);

namespace Dkh\Utility;

class Export
{
    public static function variable($value, string $indent = '  ', int $level = 0): string
    {
        switch (gettype($value)) {
            case 'array':
                if (!$value) {
                    return '[]';
                }

                $lines = [];

                $associative = array_keys($value) !== range(0, count($value) - 1);
                $next = $level + 1;

                foreach ($value as $key => $item) {
                    $key = $associative ? static::variable($key) . ' => ' : '';
                    $lines[] = str_repeat($indent, $next) . $key . static::variable($item, $indent, $next) . ',';
                }

                return '[' . "\n" . implode("\n", $lines) . "\n" . str_repeat($indent, $level) . ']';
            case 'string':
                return "'" . str_replace(['\\', "'"], ['\\\\', "\\'"], $value) . "'";
            case 'integer':
            case 'double':
                return (string)$value;
            case 'boolean':
                return $value ? 'true' : 'false';
            default:
                return var_export($value, true);
        }
    }

    public static function file($value, string $indent = '  ', int $level = 0): string
    {
        return '<?php' . "\n\n" . 'return ' . static::variable($value, $indent, $level) . ';' . "\n";
    }
}
