<?php

declare(strict_types=1);

namespace Dkh\Utility;

abstract class Facade
{
    protected static array $facade = [];

    abstract protected static function new(): mixed;

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public static function __callStatic(string $name, array $arguments)
    {
        $method = [static::instance(), $name];

        return $method(...$arguments);
    }

    public static function instance(): mixed
    {
        if (array_key_exists(static::class, static::$facade)) {
            return static::$facade[static::class];
        }

        return static::default(static::new());
    }

    protected static function default($instance): mixed
    {
        static::$facade[static::class] = $instance;

        return $instance;
    }
}
