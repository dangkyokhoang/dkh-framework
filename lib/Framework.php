<?php

declare(strict_types=1);

namespace Dkh;

use Dkh\Framework\Enhancement;
use Dkh\Framework\Handler;
use Dkh\Framework\Middleware;
use Dkh\Http\Request;
use Dkh\Http\Response;
use JetBrains\PhpStorm\NoReturn;

class Framework
{
    #[NoReturn]
    public static function start(): void
    {
        $after = Middleware::apply(Middleware::match(Request::path()));

        $handler = Handler::match(Request::method(), Request::path());

        if (!$handler) {
            Response::status(404);
            Response::end();
        }

        $enhance = Enhancement::apply(Enhancement::match(Request::path()));

        $data = $handler();

        if ($enhance) {
            $data = $enhance($data);
        }

        if ($after) {
            $data = $after($data);
        }

        Response::end($data);
    }
}
