<?php

declare(strict_types=1);

namespace Dkh\Database;

use Dkh\Database\Redisx\Connection;
use Dkh\Framework\Configuration;
use Dkh\Utility\Facade;

class Redisx extends Facade
{
    public static function new(array $options = null): Connection
    {
        return new Connection($options ?? Configuration::get('redisx'));
    }
}
