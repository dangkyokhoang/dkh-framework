<?php

declare(strict_types=1);

namespace Dkh\Database\Redisx;

use Exception;

trait Toolkit
{
    /**
     * @param string $key
     * @param array $item
     * @param int $ttl
     * @return bool
     * @throws Exception
     */
    public function save(string $key, array $item, int $ttl = 0): bool
    {
        return $this->setEx($this->bind($key, $item), $ttl, $item);
    }

    /**
     * @param string $key
     * @param array $items
     * @return bool
     * @throws Exception
     */
    public function msave(string $key, array $items): bool
    {
        return $this->mset(array_combine($this->mbind($key, $items), $items));
    }

    /**
     * @param string $key
     * @param array $item
     * @return mixed
     * @throws Exception
     */
    public function find(string $key, array $item): mixed
    {
        return $this->get($this->bind($key, $item));
    }

    /**
     * @param string $key
     * @param array[] $items
     * @return array
     * @throws Exception
     */
    public function mfind(string $key, array $items): array
    {
        return $this->mget($this->mbind($key, $items));
    }

    /**
     * @param string $key
     * @param array $item
     * @return int
     * @throws Exception
     */
    public function evict(string $key, array $item): int
    {
        return $this->mdel([$this->bind($key, $item)]);
    }

    /**
     * @param string $key
     * @param array $items
     * @return int
     * @throws Exception
     */
    public function mevict(string $key, array $items): int
    {
        return $this->mdel($this->mbind($key, $items));
    }

    /**
     * @param string $key
     * @param array $item
     * @param bool $strict
     * @return string
     * @throws Exception
     */
    public function bind(string $key, array $item, bool $strict = true): string
    {
        return preg_replace_callback(
            '/\?(\w+)/',
            function (array $match) use ($item, $strict): string {
                $name = $match[1];

                if ($strict && !array_key_exists($name, $item)) {
                    throw new Exception('property: ' . $name . ' is missing');
                }

                return (string)($item[$name] ?? '*');
            },
            $key,
        );
    }

    /**
     * @param string $key
     * @param array[] $items
     * @param bool $strict
     * @return array
     * @throws Exception
     */
    public function mbind(string $key, array $items, bool $strict = true): array
    {
        return array_map(
            fn (array $item) => $this->bind($key, $item, $strict),
            $items,
        );
    }
}
