<?php

declare(strict_types=1);

namespace Dkh\Database\Redisx;

use Exception;
use Redis;

class Connection
{
    use Toolkit;

    private ?Redis $redis = null;

    private array $options;

    private ?string $prefix;

    public function __construct(array $options)
    {
        $this->options = $options;
        $this->prefix = $options['prefix'] ?? null;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key): mixed
    {
        return $this->connection()->get($key);
    }

    public function mget(array $keys): array
    {
        return $this->connection()->mGet($keys) ?: [];
    }

    public function set(string $key, $value): bool
    {
        return $this->connection()->set($key, $value);
    }

    public function mset(array $pairs): bool
    {
        return $this->connection()->mSet($pairs);
    }

    public function setEx(string $key, int $ttl, $value): bool
    {
        if ($ttl === 0) {
            return $this->connection()->set($key, $value);
        }

        return $this->connection()->setEx($key, $ttl, $value);
    }

    public function expire(string $key, int $ttl): bool
    {
        if ($ttl === 0) {
            return $this->connection()->persist($key);
        }

        return $this->connection()->expire($key, $ttl);
    }

    public function del(array $keys): int
    {
        return $this->connection()->del($keys);
    }

    /**
     * Delete keys matching a pattern.
     * @param array $keys
     * @return int
     * @throws Exception
     */
    public function mdel(array $keys): int
    {
        $keys = $this->mscan($keys);

        if (!$keys) {
            return 0;
        }

        // TODO: if the number of keys are large,
        //       there should be multiple small deletions instead
        return $this->del($keys);
    }

    /**
     * Scan keys matching a pattern.
     * @param string $pattern
     * @return array
     * @throws Exception
     */
    public function scan(string $pattern): array
    {
        if (!str_contains($pattern, '*')) {
            return [$pattern];
        }

        $keys = [];
        $cursor = null;

        do {
            $result = $this->connection()->scan($cursor, $pattern, 1000);

            if ($result === false) {
                continue;
            }

            $keys[] = $result;
        } while ($cursor > 0);

        $prefix = $this->prefix;
        $offset = strlen($prefix);

        return array_map(
            function (string $key) use ($prefix, $offset): string {
                if (!str_starts_with($key, $prefix)) {
                    throw new Exception('error scanning keys');
                }

                return substr($key, $offset);
            },
            array_merge(...$keys),
        );
    }

    /**
     * @param array $patterns
     * @return array
     * @throws Exception
     */
    public function mscan(array $patterns): array
    {
        return array_values(array_unique(array_merge(...array_map(
            [$this, 'scan'],
            $patterns,
        ))));
    }

    public function connection(): Redis
    {
        if ($this->redis) {
            return $this->redis;
        }

        $this->redis = new Redis();

        $options = $this->options;
        $host = $options['host'] ?? null;
        $port = $options['port'] ?? null;

        if ($port) {
            $this->redis->connect($host, $port);
        } else {
            $this->redis->connect($host);
        }

        if ($this->prefix) {
            $this->redis->setOption(Redis::OPT_PREFIX, $this->prefix);
        }

        $default_options = [
            Redis::OPT_SCAN => Redis::SCAN_NORETRY,
            Redis::OPT_SERIALIZER => Redis::SERIALIZER_IGBINARY,
        ];
        $options = $options['options'] ? array_replace($default_options, $options['options']) : $default_options;

        foreach ($options as $key => $value) {
            $this->redis->setOption($key, $value);
        }

        return $this->redis;
    }

    public function __destruct()
    {
        $this->redis?->close();
    }
}
