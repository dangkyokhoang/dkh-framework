<?php

declare(strict_types=1);

namespace Dkh\Database\Db;

class Result
{
    public int $rowCount;

    public array $rows = [];

    public ?array $row = null;
}
