<?php

declare(strict_types=1);

namespace Dkh\Database\Db;

use Exception;
use mysqli;
use mysqli_result;

class Connection
{
    use Toolkit;

    private ?mysqli $connection = null;

    private array $options;

    private ?string $prefix;

    public function __construct(array $options)
    {
        $this->options = $options;
        $this->prefix = $options['prefix'] ?? null;
    }

    /**
     * @return mysqli
     * @throws Exception
     */
    public function connection(): mysqli
    {
        if ($this->connection) {
            return $this->connection;
        }

        $options = $this->options;

        $connection = new mysqli(
            $options['hostname'] ?? null,
            $options['username'] ?? null,
            $options['password'] ?? null,
            $options['database'] ?? null,
            $options['port'] ?? null,
        );

        if ($connection->connect_errno) {
            throw new Exception($connection->connect_error);
        }

        $connection->set_charset('utf8');
        $connection->query('SET SQL_MODE = ""');

        $this->connection = $connection;

        return $connection;
    }

    /**
     * @param string $query
     * @return ?Result
     * @throws Exception
     */
    public function query(string $query): ?Result
    {
        $connection = $this->connection();

        $query = $connection->query($query);

        if ($connection->error) {
            throw new Exception($connection->error . ' ' . $query);
        }

        if (!($query instanceof mysqli_result)) {
            return null;
        }

        $result = new Result();

        $result->rowCount = $query->num_rows;

        if ($result->rowCount > 0) {
            while (true) {
                $row = $query->fetch_assoc();

                if (!$row) {
                    break;
                }

                $result->rows[] = $row;
            }

            $result->row = $result->rows[0];
        }

        $query->close();

        return $result;
    }

    /**
     * @param mixed $value
     * @return string
     * @throws Exception
     */
    public function escape(mixed $value): string
    {
        return $this->connection()->real_escape_string((string)$value);
    }

    /**
     * @return int
     * @throws Exception
     */
    public function countAffected(): int
    {
        return $this->connection()->affected_rows;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function getLastId(): int
    {
        return $this->connection()->insert_id;
    }

    public function __destruct()
    {
        $this->connection?->close();
    }
}
