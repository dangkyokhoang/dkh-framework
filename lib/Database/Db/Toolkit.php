<?php

declare(strict_types=1);

namespace Dkh\Database\Db;

use Dkh\Utility\Export;
use Exception;
use JetBrains\PhpStorm\NoReturn;
use Throwable;

trait Toolkit
{
    /**
     * Build and execute a query.
     * @param string $query
     * @param array $params
     * @return ?Result
     * @throws Exception
     */
    public function execute(string $query, array $params = []): ?Result
    {
        return $this->query($this->build($query, $params));
    }

    /**
     * Build, execute and explain a query. The process exits after the execution.
     * @param string $query
     * @param array $params
     * @throws Exception
     */
    #[NoReturn]
    public function explain(string $query, array $params = []): void
    {
        echo "query definition:\n";
        var_dump($query);

        echo "params:\n";
        var_dump($params);

        $query = $this->build($query, $params);

        echo "query:\n";
        echo $query . "\n";

        $explain = $this->query('EXPLAIN ' . $query)->rows;

        echo "explain:\n";
        var_dump($explain);

        exit;
    }

    /**
     * @throws Exception
     */
    public function getLastIds(): array
    {
        $count = $this->countAffected();

        if ($count === 0) {
            return [];
        }

        $start = $this->getLastId();

        return range($start, $start + $count - 1);
    }

    /**
     * Build a query.
     * @param string $query
     * @param array $params
     * @return string
     * @throws Exception
     */
    public function build(string $query, array $params = []): string
    {
        $query = $this->prependPrefix($query);

        $spread = strpos($query, '...');

        if ($spread === false) {
            return $this->bindFragment($query, $params);
        }

        $before = substr($query, 0, $spread);
        $after = ltrim(substr($query, $spread + 3));

        preg_match(
            '/\((?:(?>[^()]+)|(?R))*\)/',
            $after,
            $matches,
            PREG_OFFSET_CAPTURE,
        );

        if (!$matches || $matches[0][1] !== 0) {
            throw new Exception('spread operator must be followed by values');
        }

        // everything but not VALUES
        $union = preg_match('/VALUES\s*$/i', $before) === 0;

        $values = $matches[0][0];
        $after = substr($after, strlen($values));

        if ($union) {
            $values = substr($values, 1, -1);
        }

        $values = array_map(
            fn ($params) => $this->bindFragment($values, $params),
            $params,
        );

        if ($union) {
            $values = '(' . implode(' UNION ALL ', $values) . ')';
        } else {
            $values = implode(', ', $values);
        }

        $before = $this->bindFragment($before, $params[0]);
        $after = $this->bindFragment($after, $params[0]);

        return $before . ' ' . $values . ($after ? ' ' . $after : '');
    }

    /**
     * @param string $query
     * @param array $params
     * @return string
     * @throws Exception
     */
    private function bindFragment(string $query, array $params): string
    {
        $query = $this->concatenate($query, $params);
        $query = $this->trim($query);

        return $this->bindParams($query, $params);
    }

    private function concatenate(string $query, array $params): string
    {
        // if the query is multiline,
        // each line is a fragment
        if (!str_contains($query, "\n")) {
            return trim($query);
        }

        $query = array_filter(array_map('trim', explode("\n", $query)));

        $fragments = [];

        foreach ($query as $fragment) {
            $matches = null;

            // if param type is empty,
            // it's a condition
            preg_match_all('/\s\?(\w+):(\s|$)/', $fragment, $matches);

            // if the fragment doesn't have a condition,
            // just take it
            if (empty($matches[1])) {
                $fragments[] = $fragment;
                continue;
            }

            $ignore = false;

            // if there has any key that doesn't exist in the params or
            //   the param is a condition that evaluates to false,
            // the fragment is ignored
            foreach ($matches[1] as $key) {
                // unset, null or false
                if (($params[$key] ?? false) === false) {
                    $ignore = true;
                    break;
                }
            }

            if ($ignore) {
                continue;
            }

            // remove void params
            $fragments[] = preg_replace('/\s\?\w+:(\s|$)/', '$1', $fragment);
        }

        return implode(' ', $fragments);
    }

    /**
     * Strips leading `AND`, `OR` and trailing commas.
     * @param string $query
     * @return string
     */
    private function trim(string $query): string
    {
        static $search = [
            // leading AND, OR
            '/((?:\(|(?<=\W)(?:WHERE|HAVING|ON))\s*)(?:AND|OR)\s*/',
            // trailing commas
            '/,(\s*(?:$|\)|(?:FROM|GROUP|WHERE|HAVING|ORDER|LIMIT|INTO)(?=\W)))/',
        ];
        static $replacement = [
            '$1',
            '$1',
        ];

        return preg_replace($search, $replacement, $query);
    }

    /**
     * @param string $query
     * @param array $params
     * @return string
     * @throws Exception
     */
    private function bindParams(string $query, array $params): string
    {
        $index = 0;

        return preg_replace_callback(
            '/\?(\w+)?(:)?(\[)?(%)?(\w+)(%)?(])?/',
            function (array $match) use ($params, &$index): string {
                $key = $match[1] !== '' ? $match[1] : $index++;

                try {
                    return $this->bindParam($match, $key, $params);
                } catch (Throwable $error) {
                    throw new Exception(
                        Export::variable([
                            'definition' => $match[0],
                            'key' => $key,
                            'params' => $params,
                        ]),
                        0,
                        $error,
                    );
                }
            },
            $query,
        );
    }

    /**
     * @param array $match
     * @param $key
     * @param array $params
     * @return string
     * @throws Exception
     */
    private function bindParam(array $match, $key, array $params): string
    {
        // throws an exception if name is specified but not followed by a colon
        if ($match[1] !== '' && $match[2] === '') {
            throw new Exception('malformed param definition');
        }

        if (!array_key_exists($key, $params)) {
            throw new Exception('param is missing');
        }

        $type = $match[5];
        // $type might be wrapped inside a [ ] to indicate that
        // an array of $type is expected
        $array = $match[3] . ($match[7] ?? '');

        $param = $params[$key];
        $wildcards = [$match[4], $match[6] ?? ''];

        switch ($array) {
            case '':
                if (strlen($type) === 1) {
                    return $this->escapeParam($type, $param, $wildcards);
                }

                $enum = $params[$type] ?? null;

                if (!$enum || !is_array($enum)) {
                    throw new Exception('malformed param definition');
                }

                return $this->escapeParam('e', $param, $enum);
            case '[]':
                if (!is_array($param)) {
                    throw new Exception('param must be an array');
                }

                $param = array_map(
                    fn ($param) => $this->escapeParam($type, $param),
                    $param,
                );
                $param = implode(',', $param);

                return '(' . $param . ')';
            default:
                throw new Exception('malformed param definition');
        }
    }

    /**
     * @param string $query
     * @return string
     * @throws Exception
     */
    private function prependPrefix(string $query): string
    {
        if (empty($this->prefix)) {
            return $query;
        }

        // tables involved in the query whose name isn't prefixed
        $tables = [];

        // STRAIGHT_JOIN and other keywords are not supported
        $pattern = '/((?:\W(?:FROM|JOIN|INTO)|^\s*UPDATE)\s+(?:(?:`\w+`|`?' . $this->prefix . '\w+`?)\s*,\s*)*)(?!' . $this->prefix . ')(\w+)/i';

        do {
            $count = 0;
            $query = preg_replace_callback(
                $pattern,
                function (array $match) use (&$tables): string {
                    return $this->prependPrefixToTable($match, $tables);
                },
                $query,
                -1,
                $count,
            );
        } while ($count > 0);

        if (!$tables) {
            return $query;
        }

        return preg_replace(
            '/(' . implode('|', array_unique($tables)) . ')(\s*\.)/',
            $this->prefix . '$1$2',
            $query,
        );
    }

    /**
     * @param array $match
     * @param array $tables
     * @return string
     * @throws Exception
     */
    private function prependPrefixToTable(array $match, array &$tables): string
    {
        $result = $match[1] . $this->prefix . $match[2];

        // if the replacement is the same as the matched string,
        // this loop might loop forever
        if ($result === $match[0]) {
            throw new Exception('possibly catastrophic pattern: ' . $result);
        }

        // to prepend prefix to table names in select expressions later
        $tables[] = $match[2];

        return $result;
    }

    /**
     * @param $type
     * @param $param
     * @param array|null $extra
     * @return string
     * @throws Exception
     */
    private function escapeParam($type, $param, array $extra = null): string
    {
        if (!is_scalar($param) && $param !== null) {
            throw new Exception('param must be a scalar value');
        }

        switch ($type) {
            case 'd':
                return (string)(float)$param;
            case 'i':
                return (string)(int)$param;
            case 's':
                $param = empty($param) ? '' : $this->escape($param);

                if ($extra) {
                    $param = $extra[0] . $param . $extra[1];
                }

                return "'" . $param . "'";
            case 'e':
                if (in_array($param, $extra)) {
                    return $param;
                }

                return $extra[array_key_first($extra)];
            default:
                throw new Exception('invalid param type: ' . $type);
        }
    }
}
