<?php

declare(strict_types=1);

namespace Dkh\Database;

use Dkh\Database\Db\Connection;
use Dkh\Framework\Configuration;
use Dkh\Utility\Facade;
use Exception;

class Db extends Facade
{
    private static array $connections = [];

    /**
     * @param array|null $options
     * @return Connection
     * @throws Exception
     */
    public static function new(array $options = null): Connection
    {
        // defaults to the first database in the configuration
        $options = $options ?? Configuration::get('db.0');

        $database = $options['database'];

        if (!$database) {
            throw new Exception('database name must be specified');
        }

        $connection = new Connection($options);

        static::$connections[$database] = $connection;

        return $connection;
    }

    /**
     * @param string $database
     * @return void
     * @throws Exception
     */
    public static function use(string $database): void
    {
        static::default(static::connect($database));
    }

    /**
     * @param string $database
     * @return Connection
     * @throws Exception
     */
    public static function connect(string $database): Connection
    {
        if (array_key_exists($database, static::$connections)) {
            return static::$connections[$database];
        }

        $options = null;
        $dbs = Configuration::get('db');

        foreach ($dbs as $db) {
            if ($db['database'] === $database) {
                $options = $db;

                break;
            }
        }

        if (!$options) {
            throw new Exception('database not configured ' . $database);
        }

        return static::new($options);
    }
}
