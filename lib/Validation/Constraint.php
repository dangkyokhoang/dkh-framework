<?php

declare(strict_types=1);

namespace Dkh\Validation;

use Dkh\Error\FormError;
use Dkh\Utility\Export;
use Exception;

class Constraint
{
    /**
     * @param array $constraint
     * @param $input
     * @return string|null
     * @throws Exception
     */
    public static function validate(array $constraint, $input): ?string
    {
        if (!$input) {
            if ($constraint['required'] ?? false) {
                return FormError::VALUE_MISSING;
            }

            return null;
        }

        if (!is_scalar($input)) {
            return FormError::BAD_INPUT;
        }

        foreach ($constraint as $name => $value) {
            switch ($name) {
                case 'required':
                    break;
                case 'max':
                    // TODO: add support for date time format
                    if ($input > $value) {
                        return FormError::RANGE_OVERFLOW;
                    }
                    break;
                case 'min':
                    if ($input < $value) {
                        return FormError::RANGE_UNDERFLOW;
                    }
                    break;
                case 'step':
                    // only supports integers
                    $min = $constraint['min'] ?? 0;

                    if (($input - $min) % $value) {
                        return FormError::STEP_MISMATCH;
                    }
                    break;
                case 'maxlength':
                    if (mb_strlen($input) > $value) {
                        return FormError::TOO_LONG;
                    }
                    break;
                case 'minlength':
                    if (mb_strlen($input) < $value) {
                        return FormError::TOO_SHORT;
                    }
                    break;
                case 'type':
                    if (!static::type($value, (string)$input)) {
                        return FormError::TYPE_MISMATCH;
                    }
                    break;
                case 'pattern':
                    if (!static::pattern($value, (string)$input)) {
                        return FormError::PATTERN_MISMATCH;
                    }
                    break;
                default:
                    throw new Exception('unknown constraint ' . $name . ' ' . Export::variable($value));
            }
        }

        return null;
    }

    /**
     * @param string $type
     * @param string $value
     * @return bool
     * @throws Exception
     */
    public static function type(string $type, string $value): bool
    {
        return match ($type) {
            'email' => filter_var($value, FILTER_VALIDATE_EMAIL) === $value,
            'url' => filter_var($value, FILTER_VALIDATE_URL) === $value,
            'number' => (
                is_numeric($value)
                && static::pattern('-?(?:\d*\.)?\d+', $value)
            ),
            'tel' => static::pattern(
                '(?:\+?[1-9]\d{0,2}\s?|0)\(?\d{3,4}\)?[\s.-]?\d{3}[\s.-]?\d{3,4}',
                $value,
            ),
            'date' => static::pattern('\d{4}-\d{2}-\d{2}', $value),
            'time' => static::pattern(
                '\d{2}:\d{2}(?::\d{2}(?:\.\d{2,3})?)?',
                $value,
            ),
            'datetime', 'datetime-local' => static::pattern(
                '\d{4}-\d{2}-\d{2}[T ]\d{2}:\d{2}(?::\d{2}(?:\.\d{2,3})?)?',
                $value,
            ),
            'color' => static::pattern(
                '#[\dA-Fa-f]{6}|#[\dA-Fa-f]{8}',
                $value,
            ),
            default => true,
        };
    }

    /**
     * @param string $pattern
     * @param string $value
     * @return bool
     * @throws Exception
     */
    public static function pattern(string $pattern, string $value): bool
    {
        $match = preg_match('/^(?:' . $pattern . ')$/u', $value);

        if ($match === false) {
            throw new Exception('error validating ' . $value . ' against ' . $pattern);
        }

        return !!$match;
    }
}
