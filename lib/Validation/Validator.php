<?php

declare(strict_types=1);

namespace Dkh\Validation;

use Dkh\Error\FormError;
use Dkh\Error\GenericError;

class Validator
{
    /**
     * @param array $data
     * @param callable $validator
     * @throws FormError
     */
    public static function validate(array $data, callable $validator): void
    {
        $payload = [];

        foreach ($data as $key => $value) {
            try {
                $message = $validator($key, $value);
            } catch (GenericError $error) {
                $message = $error->getMessage();
            }

            if ($message) {
                $payload[$key] = $message;
            }
        }

        if ($payload) {
            throw new FormError($payload);
        }
    }

    /**
     * @param array $data
     * @param array $constraint
     * @param bool $strict
     * @throws FormError
     */
    public static function constraint(array $data, array $constraint, bool $strict = true): void
    {
        if ($strict) {
            static::validate(
                $data,
                fn ($key, $value) => !array_key_exists($key, $constraint) ? FormError::BAD_INPUT : null,
            );
        }

        static::validate(
            $constraint,
            fn ($key, $constraint) => Constraint::validate(
                $constraint,
                $data[$key] ?? null,
            ),
        );
    }
}
