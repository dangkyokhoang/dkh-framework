<?php

declare(strict_types=1);

namespace Dkh\Console\Scheduler;

use Exception;

class Parser
{
    public const TIME_RANGES = [
        [0, 59],
        [0, 23],
        [1, 31],
        [1, 12],
        [0, 6],
    ];

    public function __construct(private array $crontab)
    {
    }

    /**
     * @return array
     * @throws Exception
     */
    public function run(): array
    {
        $time = array_map(
            [static::class, 'parseTime'],
            array_keys($this->crontab),
        );

        return array_map(
            fn (array $time, array $command) => [
                $time,
                is_callable($command) ? [$command] : $command,
            ],
            $time,
            $this->crontab,
        );
    }

    /**
     * @param string $time
     * @return array
     * @throws Exception
     */
    public static function parseTime(string $time): array
    {
        // * * * * *
        $fields = explode(' ', $time);

        if (count($fields) !== 5) {
            throw new Exception('invalid time ' . $time);
        }

        $fields = array_map(
            [static::class, 'parseTimeField'],
            $fields,
            static::TIME_RANGES,
        );

        // converts weekdays to be compatible with php date() function
        if ($fields[4]) {
            $fields[4] = array_map(
                fn (int $weekday) => $weekday === 0 ? 7 : $weekday,
                $fields[4],
            );
        }

        return $fields;
    }

    public static function parseTimeField(string $field, array $range): ?array
    {
        $entries = explode(',', $field);
        $entries = array_map(
            fn (string $item) => static::parseTimeRange($item, $range),
            $entries,
        );

        if (in_array(null, $entries)) {
            return null;
        }

        $entries = array_map([static::class, 'resolveTimeRange'], $entries);

        return array_unique(array_merge(...$entries));
    }

    public static function parseTimeRange(string $item, array $range): ?array
    {
        preg_match('/^([\d*]+?)(?:-(\d+))?(?:\/(\d+))?$/', $item, $matches);

        $min = $matches[1];
        $step = isset($matches[3]) ? (int)$matches[3] : null;

        if ($min === '*' && (!$step || $step === 1)) {
            return null;
        }

        if ($min === '*') {
            $min = $range[0];
            $max = $range[1];
        } else {
            $min = (int)$min;
            $max = (int)($matches[2] ?? null) ?: ($step ? $range[1] : $min);
        }

        return [$min, $max, $step ?: 1];
    }

    private static function resolveTimeRange(array $item): array
    {
        [$min, $max, $step] = $item;

        $values = [];

        for ($i = $min; $i <= $max; $i += $step) {
            $values[] = $i;
        }

        return $values;
    }

    /**
     * @param array $crontab
     * @return array
     * @throws Exception
     */
    public static function parse(array $crontab): array
    {
        return (new static($crontab))->run();
    }
}
