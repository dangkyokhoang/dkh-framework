<?php

declare(strict_types=1);

namespace Dkh\Console\Scheduler;

class Matcher
{
    public const TIME_FORMAT = ['i', 'G', 'j', 'n', 'N'];

    public static function match(array $crontab, array $now = null): array
    {
        $now = $now ?? static::now();

        $crontab = array_map(
            function (array $cron) use ($now) {
                [$time, $callable] = $cron;

                foreach ($now as $index => $value) {
                    $field = $time[$index];

                    if ($field && !in_array($value, $field)) {
                        return null;
                    }
                }

                return $callable;
            },
            $crontab,
        );

        return array_filter($crontab);
    }

    public static function now(): array
    {
        return array_map('intval', array_map('date', static::TIME_FORMAT));
    }
}
