<?php

declare(strict_types=1);

namespace Dkh\Console;

use Dkh\Console\Scheduler\Matcher;
use Dkh\Console\Scheduler\Parser;
use Exception;

class Scheduler
{
    public static function match(array $crontab, array $now = null): array
    {
        return Matcher::match($crontab, $now);
    }

    /**
     * @param array $crontab
     * @return array
     * @throws Exception
     */
    public static function parse(array $crontab): array
    {
        return Parser::parse($crontab);
    }
}
