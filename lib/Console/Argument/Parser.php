<?php

declare(strict_types=1);

namespace Dkh\Console\Argument;

class Parser
{
    private array $argv;

    private array $args = [];

    private ?string $current = null;

    private int $index = 0;

    public function __construct(array $argv = null)
    {
        $this->argv = $argv ?? $_SERVER['argv'] ?? [];
    }

    public function run(): array
    {
        foreach ($this->argv as $arg) {
            if (!str_starts_with($arg, '-')) {
                $this->close(null, $arg);
                continue;
            }

            if ($this->current) {
                $this->close();
            }

            if (!str_contains($arg, '=')) {
                $this->open($arg);
                continue;
            }

            static::close(...explode('=', $arg, 2));
        }

        if ($this->current) {
            $this->close();
        }

        return $this->args;
    }

    private function open(string $name): void
    {
        $this->current = $name;
    }

    private function close($name = null, $value = true): void
    {
        // end of the current name-value pair
        $name = $name ?? $this->current;
        $key = $name
            ? substr($name, substr($name, 1, 1) === '-' ? 2 : 1)
            : $this->index++;

        if (is_numeric($value)) {
            $value = +$value;
        }

        $this->current = null;
        $this->args[$key] = $value === 'false' ? false : $value;
    }

    public static function parse(array $argv = null): array
    {
        return (new static($argv))->run();
    }
}
