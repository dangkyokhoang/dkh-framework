<?php

declare(strict_types=1);

namespace Dkh\Console;

class Process
{
    public const UNIX = PHP_OS_FAMILY !== 'Windows';

    public static function exec(string $program, array $arguments): array
    {
        $output = [];

        exec(static::command($program, $arguments), $output);

        return $output;
    }

    public static function fork(string $program, array $arguments): void
    {
        if (static::UNIX) {
            exec('nohup ' . static::command($program, $arguments) . ' > /dev/null 2>&1 &');
        } else {
            pclose(popen('start /B ' . static::command($program, $arguments), 'r'));
        }
    }

    public static function command(string $program, array $arguments): string
    {
        $arguments = array_map([static::class, 'escape'], $arguments);
        $arguments = implode(' ', $arguments);

        return $arguments ? $program . ' ' . $arguments : $program;
    }

    public static function escape(string $arg): string
    {
        if (str_starts_with($arg, '-')) {
            return $arg;
        }

        if (static::UNIX) {
            return escapeshellarg($arg);
        }

        return '"' . str_replace('"', '\\"', $arg) . '"';
    }
}
