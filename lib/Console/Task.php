<?php

declare(strict_types=1);

namespace Dkh\Console;

use Dkh\Console\Argument\Parser;
use Dkh\Framework\Configuration;

class Task
{
    /**
     * Starts a controller in background.
     * @param callable $callable
     * @param array $arguments
     */
    public static function start(callable $callable, array $arguments = []): void
    {
        $php = PHP_BINDIR ? PHP_BINDIR . '/php' : 'php';
        $arguments = [
            __DIR__ . '/../../scripts/command.php',
            '--script=task.php',
            '--input',
            // task will have same configuration with the parent process
            json_encode([$callable, $arguments, Configuration::get()]),
        ];

        Process::fork($php, $arguments);
    }

    /**
     * Runs a controller in background task mode.
     * @see Task::start()
     */
    public static function run(): void
    {
        ['input' => $arguments] = Parser::parse();

        [$callable, $arguments, $configuration] = json_decode($arguments, true);

        Configuration::replace($configuration);

        $callable(...$arguments);
    }
}
