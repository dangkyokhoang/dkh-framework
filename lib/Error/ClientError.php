<?php

declare(strict_types=1);

namespace Dkh\Error;

use Exception;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;
use ReflectionClass;

abstract class ClientError extends Exception implements JsonSerializable
{
    protected int $status = 400;

    #[Pure]
    public function __construct(string $message = null, protected ?array $payload = null)
    {
        parent::__construct($message ?? '');
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getPayload(): ?array
    {
        return $this->payload;
    }

    public static function getType(): string
    {
        return (new ReflectionClass(static::class))->getShortName();
    }

    #[ArrayShape(['type' => 'string', 'message' => 'string', 'payload' => 'array'])]
    public function jsonSerialize(): array
    {
        $data = [
            'type' => static::getType(),
            'message' => $this->getMessage(),
        ];

        $payload = $this->getPayload();

        if ($payload !== null) {
            $data['payload'] = $payload;
        }

        return $data;
    }
}
