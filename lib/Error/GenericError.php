<?php

declare(strict_types=1);

namespace Dkh\Error;

class GenericError extends ClientError
{
}
