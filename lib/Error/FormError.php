<?php

declare(strict_types=1);

namespace Dkh\Error;

use JetBrains\PhpStorm\Pure;

class FormError extends ClientError
{
    #[Pure]
    public function __construct(array $payload)
    {
        parent::__construct(
            'invalid fields ' . implode(', ', array_keys($payload)),
            $payload,
        );
    }

    public const BAD_INPUT = 'badInput';

    public const VALUE_MISSING = 'valueMissing';

    public const RANGE_OVERFLOW = 'rangeOverflow';

    public const RANGE_UNDERFLOW = 'rangeUnderflow';

    public const STEP_MISMATCH = 'stepMismatch';

    public const TOO_LONG = 'tooLong';

    public const TOO_SHORT = 'tooShort';

    public const TYPE_MISMATCH = 'typeMismatch';

    public const PATTERN_MISMATCH = 'patternMismatch';
}
