<?php

declare(strict_types=1);

namespace Dkh\Error;

class AuthenticationError extends ClientError
{
    protected int $status = 401;
}
