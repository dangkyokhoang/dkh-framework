<?php

declare(strict_types=1);

namespace Dkh\Framework;

use Dkh\Utility\Accessor;

class Configuration extends Accessor
{
    protected static array $data = [];
}
