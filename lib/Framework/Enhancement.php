<?php

declare(strict_types=1);

namespace Dkh\Framework;

use Dkh\Framework\Enhancement\Enhancer;
use Exception;
use ReflectionAttribute;
use ReflectionMethod;

class Enhancement extends Middleware
{
    protected static array $stack = [];

    public static function apply(array $stack): ?callable
    {
        $middlewares = [];

        foreach ($stack as [$enhancer, $arguments]) {
            /**
             * @var Enhancer $enhancer
             */
            $enhancer = new $enhancer(...$arguments);

            $middlewares[] = [$enhancer, 'run'];
        }

        return parent::apply($middlewares);
    }

    public static function match(string $path): array
    {
        return static::$stack[$path] ?? [];
    }

    /**
     * @param callable $handler
     * @return array
     * @throws Exception
     */
    public static function detect(callable $handler): array
    {
        $enhancers = (new ReflectionMethod(...$handler))->getAttributes(
            Enhancer::class,
            ReflectionAttribute::IS_INSTANCEOF,
        );

        return array_map(
            fn ($enhancer) => [$enhancer->getName(), $enhancer->getArguments()],
            $enhancers,
        );
    }
}
