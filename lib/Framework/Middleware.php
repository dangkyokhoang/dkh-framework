<?php

declare(strict_types=1);

namespace Dkh\Framework;

class Middleware
{
    protected static array $stack = [];

    public static function apply(array $stack): ?callable
    {
        $after = [];

        foreach ($stack as $middleware) {
            $callback = $middleware();

            if ($callback) {
                $after[] = $callback;
            }
        }

        if (!$after) {
            return null;
        }

        return static function ($data) use ($after) {
            for ($i = count($after) - 1; $i >= 0; $i--) {
                $callback = $after[$i];

                $data = $callback($data);
            }

            return $data;
        };
    }

    public static function match(string $path): array
    {
        $result = [];

        foreach (static::$stack as $prefix => $stack) {
            if (str_starts_with($path, $prefix)) {
                $result[] = $stack;
            }
        }

        return array_merge(...$result);
    }

    public static function set(array $stack): void
    {
        static::$stack = $stack;
    }

    public static function get(): array
    {
        return static::$stack;
    }
}
