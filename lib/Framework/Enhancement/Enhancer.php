<?php

declare(strict_types=1);

namespace Dkh\Framework\Enhancement;

abstract class Enhancer
{
    abstract public function run(): ?callable;
}
