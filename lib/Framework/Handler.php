<?php

declare(strict_types=1);

namespace Dkh\Framework;

class Handler
{
    protected static array $stack = [];

    public static function match(string $method, string $path): ?callable
    {
        return static::$stack[$method][$path] ?? null;
    }

    public static function set(array $stack): void
    {
        static::$stack = $stack;
    }

    public static function get(): array
    {
        return static::$stack;
    }
}
