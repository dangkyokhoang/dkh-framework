<?php

declare(strict_types=1);

namespace Dkh\Http;

use JetBrains\PhpStorm\NoReturn;

class Response
{
    /**
     * @param ?int $status
     * @return bool|int
     */
    public static function status(?int $status = null): bool|int
    {
        if (!$status || headers_sent()) {
            return http_response_code();
        }

        return http_response_code($status);
    }

    public static function set($header, string $value = null): void
    {
        if (headers_sent()) {
            return;
        }

        if (is_array($header)) {
            foreach ($header as $name => $value) {
                static::header($name, $value);
            }
        } else {
            static::header($header, $value);
        }
    }

    public static function send($data): void
    {
        if (is_string($data)) {
            echo $data;
        } else {
            echo json_encode($data, JSON_UNESCAPED_UNICODE);
        }
    }

    private static function header(string $header, ?string $value): void
    {
        if ($value) {
            header($header . ': ' . $value);
        } else {
            header_remove($header);
        }
    }

    #[NoReturn]
    public static function end($data = null): void
    {
        if ($data !== null) {
            if (!is_string($data)) {
                static::set('content-type', 'application/json');
            }

            static::send($data);
        }

        exit;
    }
}
