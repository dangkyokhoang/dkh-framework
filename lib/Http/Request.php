<?php

declare(strict_types=1);

namespace Dkh\Http;

use Exception;

/**
 * Class Request
 * @package Dkh\Http
 * @method static string method()
 * @method static array|string|mixed query(string $key = null)
 * @method static array|mixed body(string $key = null)
 * @method static array|string|mixed headers(string $name = null)
 * @method static array files(string $key = null)
 * @method static string protocol()
 * @method static string host()
 * @method static string uri()
 * @method static string path()
 * @method static string origin()
 * @method static array ips()
 * @method static string ip()
 */
class Request
{
    private static array $accept = ['application/json'];

    private static array $trusted = ['127.0.0.1', '::ffff:127.0.0.1', '::1'];

    private static array $data = [];

    /**
     * @param string $name
     * @param array $arguments
     * @return array|string|bool
     * @throws Exception
     */
    public static function __callStatic(string $name, array $arguments)
    {
        if (!array_key_exists($name, static::$data)) {
            static::set($name, static::get($name));
        }

        $value = static::$data[$name];
        $key = $arguments[0] ?? null;

        return $key === null ? $value : $value[$key] ?? null;
    }

    /**
     * @param string $name
     * @return bool|array|string|null
     * @throws Exception
     */
    private static function get(string $name): bool|array|string|null
    {
        switch ($name) {
            case 'method':
                return $_SERVER['REQUEST_METHOD'] ?? null;
            case 'query':
                return $_GET;
            case 'body':
                $type = $_SERVER['HTTP_CONTENT_TYPE'] ?? null;

                if (!in_array($type, static::$accept)) {
                    return [];
                }

                return match ($type) {
                    'application/json' => json_decode(
                        file_get_contents('php://input'),
                        true,
                    ),
                    default => $_POST,
                };
                case 'headers':
                    if (function_exists('getallheaders')) {
                        return array_change_key_case(getallheaders(), CASE_LOWER);
                    }

                    $headers = [];

                    foreach ($_SERVER as $name => $value) {
                        if (str_starts_with($name, 'HTTP_')) {
                            $name = str_replace('_', '-', substr($name, 5));
                            $headers[strtolower($name)] = $value;
                        }
                    }

                    return $headers;
                case 'files':
                    return $_FILES;
                case 'protocol':
                    return empty($_SERVER['HTTPS']) ? 'http' : 'https';
                case 'host':
                    return $_SERVER['HTTP_HOST'] ?? null;
                case 'origin':
                    return $_SERVER['HTTP_ORIGIN'] ?? null;
                case 'uri':
                    return $_SERVER['REQUEST_URI'] ?? null;
                case 'path':
                    $path = $_SERVER['DOCUMENT_URI'] ?? null;

                    if ($path) {
                        return $path;
                    }

                    $uri = static::uri();
                    $pos = strpos($uri, '?');

                    return $pos === false ? $uri : substr($uri, 0, $pos);
                case 'ips':
                    $ips = array_filter(array_map('trim', explode(
                        ',',
                        $_SERVER['HTTP_X_FORWARDED_FOR'] ?? '',
                    )));
                    $ips[] = $_SERVER['REMOTE_ADDR'];

                    return array_reverse($ips);
                case 'ip':
                    $ips = static::ips();

                    if (isset($ips[1]) && in_array($ips[0], static::$trusted)) {
                        return $ips[1];
                    }

                    return $ips[0];
                default:
                    throw new Exception('unknown property: ' . $name);
        }
    }

    public static function set(string $name, $value): void
    {
        static::$data[$name] = $value;
    }

    public static function accept(array $accept): void
    {
        static::$accept = $accept;
    }

    public static function trust(array $ips): void
    {
        static::$trusted = $ips;
    }
}
