<?php

declare(strict_types=1);

namespace Dkh\Http;

use Dkh\Utility\Accessor;

class Session extends Accessor
{
    public const SESSION_OPTIONS = [
        'use_only_cookies' => true,
        'use_cookies' => true,
        'use_strict_mode' => true,
        'use_trans_sid' => false,
    ];

    public const COOKIE_PARAMS = [
        'httponly' => true,
        'samesite' => 'Lax',
    ];

    protected static array $data;

    private static ?string $id = null;

    public static function open(): void
    {
        // only start if session has already been created
        if (isset($_COOKIE[session_name()])) {
            static::start();
        } else {
            static::$data = [];
        }
    }

    public static function getId(): ?string
    {
        return static::$id;
    }

    public static function start(): void
    {
        if (static::$id !== null) {
            return;
        }

        session_set_cookie_params(static::COOKIE_PARAMS);
        session_start(static::SESSION_OPTIONS);

        $id = session_id();

        if (!isset($_SESSION[$id])) {
            $_SESSION[$id] = [];
        }

        static::$id = $id;
        static::$data = &$_SESSION[$id];
    }
}
