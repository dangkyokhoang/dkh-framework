<?php

declare(strict_types=1);

use Dkh\Console\Task;

function main(): void
{
    Task::run();
}
