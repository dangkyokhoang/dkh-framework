<?php

declare(strict_types=1);

use Dkh\Console\Argument\Parser;

if (is_file(__DIR__ . '/../../../autoload.php')) {
    require __DIR__ . '/../../../autoload.php';
} else {
    require __DIR__ . '/../vendor/autoload.php';
}

['bootstrap' => $bootstrap, 'script' => $script] = Parser::parse();

if ($bootstrap) {
    require $bootstrap;
}

require $script;

main();
